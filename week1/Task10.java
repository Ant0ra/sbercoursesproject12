package week1;

import java.util.Scanner;

/*
Перевод литров в галлоны.
С консоли считывается число n –  количество литров, которое нужно перевести в галлоны.
(1 литр = 0,219969 галлонна)
 */


public class Task10 {

    //private static final double GALLON_TO_LITRE = 0.219969;

    public static void main(String[] args) {

        double gallonToLitre = 0.219969;

        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        System.out.println(n * gallonToLitre);
    }
}
