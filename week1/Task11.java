package week1;

/*
Дано целое число n.
Выведите следующее за ним четное число.
При решении этой задачи нельзя использовать условную инструкцию if и циклы.

5 -> 6
10 -> 12
 */


import java.util.Scanner;

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        System.out.println(n + 2 - Math.abs(n) % 2);
    }
}
