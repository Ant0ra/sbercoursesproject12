package week1;

import java.util.Scanner;

/*
Дано двузначное число.
Вывести сначала первую цифру (единицы), затем правую (десятки)
 */


public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        int first = number / 10;
        int second = number % 10;

        System.out.println(first + " " + second);
    }
}
