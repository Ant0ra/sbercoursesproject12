package week1;

import java.util.Scanner;

/*
Дана площадь круга, нужно найти диаметр окружности и длину окружности.

S = PI * r^2 -> r = d / 2 ->
S = PI * (d^2 / 4)
d = Math.sqrt(S * 4  / PI)
PI = L : d -> L = PI * d
*/

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double s = scanner.nextDouble();

        double d = Math.sqrt(s * 4  / Math.PI);
        double l = Math.PI * d;

        System.out.println(d);
        System.out.println(l);
    }
}
