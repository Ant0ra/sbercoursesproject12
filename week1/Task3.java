package week1;

import java.util.Scanner;

/*
Напишите программу, которая получает два числа с плавающей точкой х и у в
аргументах командной строки и выводит евклидово расстояние от точки (х, у) до точки (0, 0)
 */


public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();

        System.out.println(Math.sqrt(x * x + Math.pow(y, 2)));
    }
}
