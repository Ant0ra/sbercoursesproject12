package week2;

import java.util.Scanner;

/*
Считать данные из консоли о типе номера отеля.
1 VIP, 2 Premium 3 Comfort.
Вывести цену номера VIP 125 Premium 110 Standard 100
1 -> 125
2 -> 110
 */

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int roomType = scanner.nextInt();

        switch (roomType) {
            case 1 -> System.out.println(125);
            case 2 -> System.out.println(110);
            case 3 -> System.out.println(100);
            default -> System.out.println("Неизвестный номер");
        }

    }
}
