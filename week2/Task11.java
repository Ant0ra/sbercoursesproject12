package week2;

import java.util.Scanner;

/*
Дана строка и паттерн, заменить паттерн на паттерн, состоящий из заглавных символов
Входные данные
Hello world
ld
Выходные данные
Hello worLD
 */

public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        String pattern = scanner.nextLine();

        System.out.println(str.replace(pattern, pattern.toUpperCase()));
    }
}
