package week2;

import java.util.Scanner;

//разбить на 2 задачи
/*
Даны два числа a и b. Проверить утверждение, что только одно из них нечетное.
10 10 false
11 10 true
10 11 true
11 11 false
 */

public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();


        //Проверить увтерждение, что хотя бы одно из них четное
//        if (a % 2 != 0 || b % 2 != 0) {
//            System.out.println(true);
//        } else {
//            System.out.println(false);
//        }

        //Проверить утверждение, что только одно из них нечетное.
//        if (a % 2 != 0 ^ b % 2 != 0) {
//            System.out.println(true);
//        } else {
//            System.out.println(false);
//        }

        //то же самое (только одно нечетное)
        boolean aIsOdd = a % 2 != 0;
        boolean bIsOdd = b % 2 != 0;

        if ((aIsOdd && !bIsOdd) || (!aIsOdd && bIsOdd)) {
            System.out.println(true);
        } else {
            System.out.println(false);
        }


    }
}
