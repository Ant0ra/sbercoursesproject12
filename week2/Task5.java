package week2;

import java.util.Scanner;

/*
Не задача. Пример, как можно корректно считать String после считывания числа.
 */

public class Task5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        scanner.nextLine(); //для считывания перевода строки
        String s = scanner.nextLine();

        System.out.println(a);
        System.out.println(s);
    }
}
