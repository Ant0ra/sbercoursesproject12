package week7.task5;

import java.util.Arrays;

/*
Примитивная реализация ArrayList.
Массив только int, из методов только добавлять элемент,
получать size и увеличивать капасити, когда добавляется новый.
 */

public class MyArrayList {

    private int[] arr;
    private int size;
    private int capacity;

    private static final int DEFAULT_CAPACITY = 5;

    public MyArrayList() {
        this.capacity = DEFAULT_CAPACITY;
        this.size = 0;
        this.arr = new int[DEFAULT_CAPACITY];
    }

    public MyArrayList(int initialCapacity) {
        this.capacity = initialCapacity;
        this.size = 0;
        this.arr = new int[initialCapacity];
    }

    public void add(int elem) {
        if (size >= capacity) {
            capacity = capacity * 2;
            arr = Arrays.copyOf(arr, capacity);
        }

        arr[size] = elem;
        size++;
    }

    public int get(int idx) {
//        if (idx < 0 || idx >= size) {
//            System.out.println("Impossible");
//            return -1;
//        }
        return arr[idx];
    }

    public int size() {
        return size;
    }
}
