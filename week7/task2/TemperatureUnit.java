package week7.task2;

public enum TemperatureUnit {
    CELSIUS,
    FAHRENHEIT
}
