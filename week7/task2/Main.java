package week7.task2;

public class Main {

    public static void main(String[] args) {
        Thermometer thermometer = new Thermometer(-25, TemperatureUnit.CELSIUS);
        Thermometer thermometer1 = new Thermometer(-25, TemperatureUnit.FAHRENHEIT);
        System.out.println(thermometer.getTempCelsius());
        System.out.println(thermometer.getTempFahrenheit());
    }
}
