package week7.task3;

/*Робот.
Умеет повернуть влево, повернуть вправо, идти на 1 шаг.
Несколько конструкторов, хранение координат, вывод потом координат на экран.
*/

public class Robot {

    private int x;
    private int y;
    private Direction direction; //0 -- up, 1 -- right, 2 -- bottom, 3 -- left

    public Robot() {
        this.x = 0;
        this.y = 0;
        direction = Direction.UP;
    }

    public Robot(int x, int y) {
        this.x = x;
        this.y = y;
        direction = Direction.UP;
    }

    public Robot(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public void go() {
        switch (direction) {
            case UP: {
                y++;
                break;
            }
            case RIGHT: {
                x++;
                break;
            }
            case BOTTOM: {
                y--;
                break;
            }
            case LEFT: {
                x--;
                break;
            }
        }
        printCoordinates();
    }

    //0 -- up, 1 -- right, 2 -- bottom, 3 -- left
    public void turnLeft() {
        //this.direction = direction == 0 ? 3 : direction - 1;
        this.direction = Direction.ofNumber((this.direction.number + 3) % 4);
    }

    public void turnRight() {
        this.direction = Direction.ofNumber((this.direction.number + 1) % 4);
    }

    public void printCoordinates() {
        System.out.println("(x, y) == " + x + ", " + y);
    }
}
