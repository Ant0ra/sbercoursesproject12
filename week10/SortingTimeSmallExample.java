package week10;

import java.util.Arrays;

public class SortingTimeSmallExample {


    public static void main(String[] args) {

        int[] array = new int[]{5, 2, 3, 9, 1, 2};
        int[] arrayToSort = Arrays.copyOf(array, array.length);
        arraysSort(arrayToSort);
        arrayToSort = Arrays.copyOf(array, array.length);
        bubbleSort(arrayToSort);
        arrayToSort = Arrays.copyOf(array, array.length);
        insertionSort(arrayToSort);
        arrayToSort = Arrays.copyOf(array, array.length);
        radixSort(arrayToSort);
        arrayToSort = Arrays.copyOf(array, array.length);
        quickSort(arrayToSort);
    }


    public static void arraysSort(int[] array) {
        long startTime = System.nanoTime();
        Arrays.sort(array);
        long stopTime = System.nanoTime();
        System.out.println("ArraysSort: " + (stopTime - startTime));
    }

    public static void bubbleSort(int[] array) {
        long startTime = System.nanoTime();
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length; j++) {
                if (array[j - 1] > array[j]) {
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }
        long stopTime = System.nanoTime();
        System.out.println("BubbleSort: " +  (stopTime - startTime));
    }

    public static void insertionSort(int[] array) {
        long startTime = System.nanoTime();
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i;
            while (j > 0 && array[j - 1] > current) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = current;
        }
        long stopTime = System.nanoTime();
        System.out.println("InsertionSort: " +  (stopTime - startTime));
    }


    public static void radixSort(int[] array) {
        long startTime = System.nanoTime();
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        int[] counters = new int[max + 1];
        for (int i = 0; i < array.length; i++) {
            counters[array[i]] += 1;
        }

        int idx = 0;
        for (int i = 0; i < counters.length; i++) {
            for (int j = 0; j < counters[i]; j++) {
                array[idx] = i;
                idx++;
            }
        }
        long stopTime = System.nanoTime();
        System.out.println("RadixSort: " +  (stopTime - startTime));
    }


    public static void quickSort(int[] array) {
        long startTime = System.nanoTime();
        quickSort(array, 0, array.length - 1);
        long stopTime = System.nanoTime();
        System.out.println("QuickSort: " + (stopTime - startTime));
    }


    public static void quickSort(int[] arr, int from, int to) {
        if (from >= to) {
            return;
        }
        int divideIndex = partition(arr, from, to);
        quickSort(arr, from, divideIndex - 1);
        quickSort(arr, divideIndex, to);

    }


    private static int partition(int[] array, int from, int to) {
        int rightIndex = to;
        int leftIndex = from;

        int pivot = array[(from + to) / 2];
        while (leftIndex <= rightIndex) {

            while (array[leftIndex] < pivot) {
                leftIndex++;
            }

            while (array[rightIndex] > pivot) {
                rightIndex--;
            }

            if (leftIndex <= rightIndex) {
                int tmp = array[rightIndex];
                array[rightIndex] = array[leftIndex];
                array[leftIndex] = tmp;
                leftIndex++;
                rightIndex--;
            }
        }
        return leftIndex;
    }
}
