package week10;

import java.util.Arrays;

//https://www.sortvisualizer.com/
//https://www.youtube.com/watch?v=semGJAJ7i74&list=PLOmdoKois7_FK-ySGwHBkltzB11snW7KQ&index=3
public class SortingExample {


    public static void main(String[] args) {
        int[] array = new int[]{5, 2, 3, 9, 1, 2};
        quickSort(array);
        //Arrays.sort(array);
        //System.out.println(Arrays.toString(array));
    }

    //Сортировка пузырьком O(N^2)
    //5, 2, 3, 9, 1, 2
    public static void bubbleSort(int[] array) {
        System.out.println(Arrays.toString(array));
        for (int i = 0; i < array.length; i++) {
            for (int j = 1; j < array.length - i; j++) {
                if (array[j - 1] > array[j]) {
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
                System.out.println(Arrays.toString(array));
            }
            System.out.println("-----");
        }
    }

    public static void bubbleSort2(int[] array) {
        boolean isSorted = false;
        while (!isSorted) {
            System.out.println(Arrays.toString(array));
            isSorted = true;
            for (int j = 1; j < array.length; j++) {
                if (array[j - 1] > array[j]) {
                    int temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                    isSorted = false;
                }
                System.out.println(Arrays.toString(array));
            }
            System.out.println("-----");
        }
    }


    //Сортировка вставками O(N^2)
    //5, 2, 3, 9, 1, 2
    public static void insertionSort(int[] array) {
        System.out.println(Arrays.toString(array));
        for (int i = 1; i < array.length; i++) {
            int current = array[i];
            int j = i;
            while (j > 0 && array[j - 1] > current) {
                array[j] = array[j - 1];
                j--;
            }
            array[j] = current;
            System.out.println(Arrays.toString(array));
            System.out.println("----");
        }
    }


    /*
    Сортировка подсчетом O(N)
    + доп память O(max)!
    + только для integer!
     */
    //5, 2, 3, 9, 1, 2
    public static void radixSort(int[] array) {
        int max = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
            }
        }

        int[] counters = new int[max + 1];
        for (int i = 0; i < array.length; i++) {
            counters[array[i]] += 1;
        }

        int idx = 0;
        for (int i = 0; i < counters.length; i++) {
            for (int j = 0; j < counters[i]; j++) {
                array[idx] = i;
                idx++;
            }
        }
    }


    //Быстрая сортировка O(N*Log(N))
    public static void quickSort(int[] array) {
        quickSort(array, 0, array.length - 1);
    }

    public static void quickSort(int[] array, int from, int to) {
        if (from >= to) {
            return;
        }
        int divideIndex = partition(array, from, to);
        quickSort(array, from, divideIndex - 1);
        quickSort(array, divideIndex, to);

    }

    private static int partition(int[] array, int from, int to) {
        int rightIndex = to;
        int leftIndex = from;


        System.out.println(Arrays.toString(array));
        int pivot = array[(from + to) / 2];
        System.out.println(leftIndex);
        System.out.println(rightIndex);
        System.out.println(pivot);
        while (leftIndex <= rightIndex) {

            while (array[leftIndex] < pivot) {
                leftIndex++;
            }

            while (array[rightIndex] > pivot) {
                rightIndex--;
            }

            if (leftIndex <= rightIndex) {
                int tmp  = array[rightIndex];
                array[rightIndex] = array[leftIndex];
                array[leftIndex] = tmp;
                leftIndex++;
                rightIndex--;
                System.out.println(Arrays.toString(array));
            }
        }
        System.out.println(Arrays.toString(array));
        System.out.println("----");
        return leftIndex;
    }


}
