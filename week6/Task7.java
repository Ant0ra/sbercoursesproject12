package week6;

import java.util.Random;
import java.util.Scanner;

/*
На вход передается N — количество мест в одном ряду кинотеатра и M — количество рядов.
Необходимо заполнить кинотеатр размера N на M случайным заполнением (0 — свободное место, 1 — занятое).

Петя со своей подругой хочет купить два билета в кино рядом.
Необходимо вывести на экран предзаполненные места кинотеатра и после этого проверить,
найдутся ли подходящие места для Пети и его подруги.

Подходящими местами являются два свободных места рядом в одном ряду.

3 4
->
0 0 1
1 1 0
0 0 1
0 1 0
true

2 2
->
1 1
1 1
false

 */

public class Task7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m = scanner.nextInt();

        Random random = new Random();

        int[][] a = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = random.nextInt(2);
            }
        }

        boolean result = false;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n - 1; j++) {
                if (a[i][j] == 0 && a[i][j + 1] == 0) {
                    result = true;
                }
            }
        }

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }

        System.out.println(result);
    }
}
