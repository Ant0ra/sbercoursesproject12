package week6;

import java.util.Scanner;

/*
На вход передается N — высота двумерного массива и M — его ширина.
Затем передается сам массив.

Необходимо сохранить в одномерном массиве суммы чисел каждого столбца и вывести их на экран.

2 2
10 20
5 7
->
15 27


3 1
30
42
15
->
87
 */

public class Task4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt(); //количество строк
        int m = scanner.nextInt(); //количество столбцов

        int[][] a = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                a[i][j] = scanner.nextInt();
            }
        }

        int[] res = new int[m];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i] += a[j][i];
            }
        }

        for (int i = 0; i < m; i++) {
            System.out.println(res[i]);
        }
    }
}
