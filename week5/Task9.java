package week5;

/*
На вход подается число N — количество пар чисел, переданных далее (описание элементов массива).
Далее N строк состоящих из двух чисел: количества элементов и сам элемент.

Вывести массив, соответствующий данному описанию.

3
2 7
4 1
1 6
->
7 7 1 1 1 1 6
 */

import java.util.Scanner;

public class Task9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] count = new int[n];
        int[] elem = new int[n];

        int size = 0;
        for (int i = 0; i < n; i++) {
            count[i] = scanner.nextInt();
            elem[i] = scanner.nextInt();
            size += count[i];
        }

        int[] res = new int[size];

        int k = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < count[i]; j++) {
                res[k++] = elem[i];
            }
        }

        for (int i = 0; i < size; i++) {
            System.out.print(res[i] + " ");
        }
    }
}
