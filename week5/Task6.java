package week5;

import java.util.Scanner;

/*
 На вход подается число N — длина массива.
 Затем передается отсортированный по возрастанию массив целых различных чисел из N элементов.
 После этого число M.


Найти в массиве все пары чисел, которые в сумме дают число M и вывести их на экран.
Если таких нет, то вывести -1.

5
1 2 3 4 7
5
->
1 4
2 3

 */

public class Task6 {

    public static void main(String[] args) {
        int n;
        int[] array;


        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();

        array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        int m = scanner.nextInt();

        boolean exists = false;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (array[i] + array[j] == m) {
                    System.out.println(array[i] + " " + array[j]);
                    exists = true;
                }
                if (array[i] + array[j] > m) {
                    break;
                }
            }
        }
        if (!exists) {
            System.out.println(-1);
        }
    }

}
