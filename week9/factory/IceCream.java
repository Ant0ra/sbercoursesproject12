package week9.factory;

public interface IceCream {

    void printIngredients();
}
