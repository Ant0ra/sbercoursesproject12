package week9.factory;

public class IceCreamFactory {

    public IceCream getIceCream(IceCreamType type) {
        switch (type) {
            case CHERRY -> {
                return new CherryIceCream();
            }
            case VANILLA -> {
                return new VanillaIceCream();
            }
            case CHOCOLATE -> {
                return new ChocolateIceCream();
            }
        }
        return null;
    }
}
