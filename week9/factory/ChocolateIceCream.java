package week9.factory;

public class ChocolateIceCream implements IceCream {

    @Override
    public void printIngredients() {
        System.out.println("Chocolate, Cream, Ice, Love");
    }
}
