package week9.arraylistexample;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
//        List<Integer> arrayList = new ArrayList<>();
//        arrayList.add(1);
//        arrayList.add(1);
//        arrayList.add(1);
//        arrayList.add(1);
//        arrayList.add(1);
//        arrayList.add(1);
//        System.out.println(arrayList.get(0));
//        System.out.println(arrayList.size());

        ArrayList<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW"));
        cars.add(new Car("VOLVO"));
        cars.add(new Car("TOYOTA"));

//        for (Car car : cars) {
//           if (car.getModel().equals("TOYOTA")) {
//               cars.remove(car);
//           }
//        }

//        Iterator<Car> iterator = cars.iterator();
//        while (iterator.hasNext()) {
//            Car car = iterator.next();
//            if (car.getModel().equals("TOYOTA")) {
//                iterator.remove(); //неправильно: cars.remove(car);
//            }
//        }

        int idxToDelete = -1;
        for (int i = 0; i < cars.size(); i++) {
            Car car = cars.get(i);
            if (car.getModel().equals("TOYOTA")) {
                idxToDelete = i;
            }
        }

        if (idxToDelete != -1) {
            cars.remove(idxToDelete);
        }

        for (int i = 0; i < cars.size(); i++) {
            System.out.println(cars.get(i).getModel());
        }
//        for (Car car : cars) {
//            System.out.println(car.getModel());
//        }
    }
}
