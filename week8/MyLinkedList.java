package week8;


public class MyLinkedList {

    private int size;
    private Node head;

    private static class Node {
        int value;
        Node next;

        Node(int value, Node next) {
            this.value = value;
            this.next = next;
        }
    }

    public MyLinkedList() {
        head = null;
        size = 0;
    }

    public int get(int index) {
        if (index < 0 || index > size) {
            return -1; //future: обработку с помощью exception + log4j
        }

        Node node = head;
        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.value;
    }

    public void addAtHead(int val) {
        Node newHead = new Node(val, head);
        head = newHead;
        size++;
    }

    public void addAtTail(int val) {
        if (size > 0) {
            Node node = head;
            for (int i = 0; i < size - 1; i++) {
                node = node.next;
            }
            node.next = new Node(val, null);
        } else {
            head = new Node(val, null);
        }
        size++;
    }

    public void addAtIndex(int index, int val) {
        if (index < 0 || index > size) {
            return;
        }

        if (index == 0) {
            addAtHead(val);
            return;
        }

        if (index == size) {
            addAtTail(val);
            return;
        }

        Node node = head;
        for (int i = 0; i < index - 1; i++) {
            node = node.next;
        }

        Node newNode = new Node(val, node.next);
        node.next = newNode;
        size++;
    }

    public void deleteAtIndex(int index) {
        if (index < 0 || index > size) {
            return;
        }

        if (index == 0) {
            head = head.next;
            size--;
            return;
        }

        Node node = head;
        for (int i = 0; i < index - 1; i++) {
            node = node.next;
        }

        if (index == size - 1) {
            node.next = null;
        } else {
            node.next = node.next.next;
        }
        size--;
    }
}

/*
         ArrayList    MyLinkedList
get        О(1)         О(n) (head O(1))
remove     О(n)         О(n) (head O(1))
addIdx     О(n)         О(n)
addFirst   О(n)         O(1)
addLast    О(1)*        O(n)
 */
