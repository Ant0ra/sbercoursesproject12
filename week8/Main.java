package week8;

import java.io.*;
import java.util.Scanner;

public class Main {

//    public static int sum(int a, int... numbers) {
//
//    }

    public static void main(String[] args) throws IOException {

        System.out.println(String.format("Бла бла бла %s бла бла %d", "sdf", 1));


        Scanner scanner = new Scanner(new File("input.txt"));

        String[] weekDays = new String[3];
        int i = 0;
        while (scanner.hasNext()) {
            weekDays[i++] = scanner.nextLine();
        }

        Writer wr = new FileWriter("output.txt");
        for (String weekDay : weekDays) {
            String str = "Порядковый номер дня недели " + weekDay + " равен = " + WeekDays.ofName(weekDay).dayNumber + "\n";
            wr.write(str);
        }

        /*
        StringBuilder str = new StringBuilder();
        for (String weekDay : weekDays) {
            str.append("Порядковый номер дня недели ").append(weekDay).append(" равен = ").append(WeekDays.ofName(weekDay).dayNumber).append("\n");
        }
        wr.write(str.toString());
         */

        wr.close();
    }
}
