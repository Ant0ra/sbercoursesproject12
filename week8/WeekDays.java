package week8;

public enum WeekDays {

    MONDAY(1, "понедельник"),
    TUESDAY(2, "вторник"),
    WEDNESDAY(3, "среда"),
    THURSDAY(4, "четверг"),
    FRIDAY(5, "пятница"),
    SATURDAY(6, "суббота"),
    SUNDAY(7, "воскресенье");

    public final int dayNumber;
    public final String name;

    WeekDays(int dayNumber, String name) {
        this.dayNumber = dayNumber;
        this.name = name;
    }

    public static WeekDays ofNumber(int dayNumber) {
        for (WeekDays weekDays : values()) {
            if (weekDays.dayNumber == dayNumber) {
                return weekDays;
            }
        }
        return null;
    }

    public static WeekDays ofName(String name) {
        for (WeekDays weekDays : values()) {
            if (weekDays.name.equals(name)) {
                return weekDays;
            }
        }
        return null;
    }
}
